===================
HVL CCB GUI Example
===================


PyQt5 and PyQtGraph-based example of GUI using HVL common code basis `hvl-ccb`_ package.

.. _`hvl-ccb`: https://pypi.org/project/hvl-ccb/

This sample GUI is based on Henrik Menne's HVL CCB GUI Example experiment
GUI.

* Free software: GNU General Public License v3

Overview
--------

.. image:: https://gitlab.com/ethz_hvl/hvl_ccb-gui-example/raw/master/docs/img/snapshot.png
    :target: https://gitlab.com/ethz_hvl/hvl_ccb-gui-example/raw/master/docs/img/snapshot.png


Features
--------

* [ ] experiment with a LabJack device in a DEMO mode
* [ ] min. win size
* buttons:
    * [ ] start/stop experiment
    * live changes:
        * [ ] numeric input field: voltage range
        * [ ] slider: AIO output value
        * [ ] toggle button: DIO valve change
    * [ ] dummy UI Tab with extra space
    * [ ] dummy pop-up window
* file management: data & config files
    * [ ] output directory: data & config (JSON)
    * [ ] load input config from JSON file
    * [ ] user inputs to edit default config values and save
* [ ] live-updated graph

Credits
-------

This package was created with Cookiecutter_ and the
`audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
