.. highlight:: shell

============
Installation
============


Stable release
--------------

To install HVL CCB GUI Example, run this command in your terminal:

.. code-block:: console

    $ pip install hvl_ccb-gui-example

This is the preferred method to install HVL CCB GUI Example, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for HVL CCB GUI Example can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/ethz_hvl/hvl_ccb-gui-example

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://gitlab.com/ethz_hvl/hvl_ccb-gui-example/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://gitlab.com/ethz_hvl/hvl_ccb-gui-example
.. _tarball: https://gitlab.com/ethz_hvl/hvl_ccb-gui-example/tarball/master
