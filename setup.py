#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "Click>=7.1.2",
    "pyqtgraph>=0.11.0",
    "pyqt5>=5.15.0",
    "hvl_ccb>=0.3.5",
]

setup(
    author="Mikołaj Rybiński",
    author_email="mikolajr@ethz.ch",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    description=(
        "PyQt5 and PyQtGraph-based example of GUI using HVL common code basis "
        "`hvl-ccb` package."
    ),
    entry_points={
        "console_scripts": ["hvl_ccb_gui_example=hvl_ccb_gui_example.cli:main",],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="HVL, hvl_ccb, PyQt, GUI",
    name="hvl_ccb_gui_example",
    packages=find_packages(include=["hvl_ccb_gui_example"]),
    test_suite="tests",
    url="https://gitlab.com/ethz_hvl/hvl_ccb_gui_example",
    version="0.1.0",
    zip_safe=False,
)
