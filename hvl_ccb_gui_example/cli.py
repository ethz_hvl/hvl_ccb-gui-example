#  Copyright (c) 2020 ETH Zurich, SIS ID and HVL D-ITET
#
"""Console script for hvl_ccb_gui_example."""
import sys
import click


@click.command()
@click.option(
    "--args",
    "-a",
    multiple=True,
    help="(Multiple) arguments passed directly to QApplication constructor",
)
def main(args):
    """Run the GUI application"""
    from .gui import app

    return app.run(list(args))


if __name__ == "__main__":
    print(sys.argv)
    sys.exit(main())  # pragma: no cover
