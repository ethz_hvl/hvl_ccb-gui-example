#  Copyright (c) 2020 ETH Zurich, SIS ID and HVL D-ITET
#
"""Top-level package for Python Boilerplate."""

__author__ = """Mikołaj Rybiński"""
__email__ = "mikolajr@ethz.ch"
__version__ = "0.1.0"
