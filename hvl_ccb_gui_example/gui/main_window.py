#  Copyright (c) 2020 ETH Zurich, SIS ID and HVL D-ITET
#
from PyQt5.QtWidgets import QMainWindow


class MainWindow(QMainWindow):
    """
    GUI main window
    """

    def __init__(self, title, width: int = 1280, height: int = 720):
        super().__init__()

        self.title = title
        self.left = self.top = 0
        self.width = width
        self.height = height

    def show(self) -> None:
        """
        Set title, geometry etc. and show the window.
        """
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        super().show()
