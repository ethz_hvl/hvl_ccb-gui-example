#  Copyright (c) 2020 ETH Zurich, SIS ID and HVL D-ITET
#
"""Module to run the application"""
import os
from typing import List

from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication

from .. import __version__ as APP_VERSION
from ..gui import app_resources  # noqa: F401

from .main_window import MainWindow


APP_NAME = "HVL CCB GUI Example"
"""Application name"""


def run(args: List[str]) -> int:
    """Run the application.

    :param args: String arguments passed directly to QApplication constructor.
    :return: Integer exit code
    """

    # Init application
    app = QApplication(args)
    app.setApplicationName(APP_NAME)
    app.setApplicationVersion(APP_VERSION)
    _set_icon(app)

    # Init and show main window
    window = MainWindow(APP_NAME)
    window.show()

    # Start the event loop
    return app.exec_()


def _set_icon(app: QApplication) -> None:
    """Set application icon.

    :param app: QApplication to set icon of
    """
    # Set AppID to show icon in Windows taskbar
    # Source: https://stackoverflow.com/a/1552105
    if os.name == 'nt':  # Windows
        import ctypes
        # https://docs.microsoft.com/en-us/windows/win32/shell/appids?redirectedfrom=MSDN#how-to-form-an-application-defined-appusermodelid
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
            f"ETHZ.{APP_NAME.replace(' ', '_')}.{APP_VERSION}"
        )

    icon = QIcon()
    path = ":/images/logo-256.png"
    icon.addFile(path, QSize(256, 256))
    app.setWindowIcon(icon)
