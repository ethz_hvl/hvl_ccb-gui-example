#  Copyright (c) 2020 ETH Zurich, SIS ID and HVL D-ITET
#
"""Tests for `hvl_ccb_gui_example` package."""

# import pytest

from click.testing import CliRunner

# from hvl_ccb_gui_example import gui
from hvl_ccb_gui_example import cli


def test_gui():
    """Sample pytest test function with the pytest fixture as an argument."""
    # TODO:


def test_cli():
    """Test the CLI."""
    runner = CliRunner()
    # TODO: test that `result = runner.invoke(cli.main)` runs the app (but how to quit?)
    help_result = runner.invoke(cli.main, ["--help"])
    assert help_result.exit_code == 0
    assert "--help" in help_result.output
    assert "Show this message and exit." in help_result.output
