=======
Credits
=======

Development Lead
----------------

* Mikołaj Rybiński <mikolajr@ethz.ch>

Contributors
------------

* Henrik Menne <menneh@ethz.ch>
