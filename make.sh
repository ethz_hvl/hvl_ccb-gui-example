#!/usr/bin/env bash
#  Copyright (c) 2020 ETH Zurich, SIS ID and HVL D-ITET
#

# Shell script used as a "Makefile-like" helper tool for running tests, checking style,
# static type checking, generating docs etc.
# This script can be run using a standard installation of Git on Windows in the
# Git-BASH shell.

# ------------------------------------
# Python scripts
# ------------------------------------

BROWSER_PYSCRIPT="
import os, webbrowser, sys

webbrowser.open(os.path.abspath(sys.argv[1]))
"

OPEN_PYSCRIPT="
import os, subprocess, sys

filepath = sys.argv[1]
if sys.platform.startswith('darwin'): # macOS
    subprocess.call(('open', filepath))
elif os.name == 'posix': # Linux etc.
    subprocess.call(('xdg-open', filepath))
elif os.name == 'nt': # Windows
    filepath = os.path.join(*filepath.split('/'))
    os.startfile(filepath)
"

PRINT_HELP_PYSCRIPT="
import re, sys

for line in sys.stdin:
	match = re.match(r'^\s*\"([a-zA-Z_-]+)\"\).*?## (.*)$', line)
	if match:
		target, help = match.groups()
		print('%-20s %s' % (target, help))
"

function print_help() {
    echo "This is a small helper script to trigger frequently used command sets"
    echo "for when make tool is not available. Usage:"
    echo ""
    echo "    ./make.sh COMMANDS"
    echo ""
    echo "Multiple commands are executed in the sequence given."
    echo ""
    echo "COMMANDS:"
    echo ""
    cat $0 | python -c "${PRINT_HELP_PYSCRIPT}"
}


function clean_build() {
    rm -fr build/
    rm -fr dist/
    rm -fr .eggs/
    find . -name '*.egg-info' -exec rm -fr {} +
    find . -name '*.egg' -exec rm -f {} +
}


function clean_pyc() {
    find . -name '*.pyc' -exec rm -f {} +
    find . -name '*.pyo' -exec rm -f {} +
    find . -name '*~' -exec rm -f {} +
    find . -name '__pycache__' -exec rm -fr {} +
}


function clean_test() {
    rm -fr .tox/
    rm -fr .pytest_cache
}


function docs_api() {
    # generate sphinx API docs
    rm -f docs/hvl_ccb_gui_example*.rst
    sphinx-apidoc -o docs/ hvl_ccb_gui_example
    # fix submodules inlined headings: change "-" underline to "~"
    for f in $(ls -1 docs/hvl_ccb_gui_example*.rst); do for n in $(grep -n "hvl\\\_ccb.* module" "${f}" | cut -d : -f 1); do sed -i.tmp "$((n + 1))s/-/~/g" "${f}"; done done
    rm -f docs/hvl_ccb_gui_example*.rst.tmp
}


# ------------------------------------
# for loop over all keywords given
# ------------------------------------

for KEYWORD in "$@"; do
    case "${KEYWORD}" in
        "help") ## print help
            print_help
            ;;

        "clean") ## remove all build, test, coverage and Python artifacts
            clean_build
            clean_pyc
            clean_test
            ;;

        "clean-build") ## remove build artifacts
            clean_build
            ;;

        "clean-pyc") ## remove Python file artifacts
            clean_pyc
            ;;

        "clean-test") ## remove test and coverage artifacts
            clean_test
            ;;

        "style") ## check style with flake8
            flake8 hvl_ccb_gui_example tests
            ;;

        "format") ## auto-format with black
            black hvl_ccb_gui_example tests
            ;;

        "type") ## static code check using typing hints with mypy
            mypy --show-error-codes hvl_ccb_gui_example
            ;;

        "test") ## run tests with the default Python
            py.test
            ;;

        "test-all") ## run tests on every Python version with tox
            tox
            ;;

        "docs") ## generate sphinx HTML docs and open browser
            docs_api
            rm -rf docs/_build
            python -msphinx -M html docs/ docs/_build
            python -c "${BROWSER_PYSCRIPT}" docs/_build/html/index.html
            ;;

        "docs-pdf") ## generate sphinx PDF docs
            docs_api
            rm -rf docs/_build
            python -msphinx -M latexpdf docs/ docs/_build
            python -c "${OPEN_PYSCRIPT}" docs/_build/latex/hvl_ccb_gui_example.pdf
            ;;

        "app-resources") ## generate PyQt5 app resources module
            pyrcc5 -o hvl_ccb_gui_example/gui/app_resources.py resources/resources.qrc
            ;;

        *)
            # default action
            print_help
            ;;

    esac
done
